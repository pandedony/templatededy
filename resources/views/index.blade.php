@extends('layouts.main')
@section('container')
<section id="about">
<div class="container col-lg px-4 py-5">
  <div class="row flex-lg-row-reverse align-items-center g-5 py-5">
    <div class="col-10 col-sm-8 col-lg-6 bg-light">
      <div class="image-right">
      <img src="/img/tema.png" class="d-block mx-lg-auto img-fluid" alt="Bootstrap Themes" width="700" height="500" loading="lazy">
    </div>
  </div>
    <div class="col-lg-6">
      <h1 class="display-5 fw-bold lh-1 mb-3">Responsive left-aligned hero with image</h1>
      <p class="lead">Quickly design and customize responsive mobile-first sites with Bootstrap, the world’s most popular front-end open source toolkit, featuring Sass variables and mixins, responsive grid system, extensive prebuilt components, and powerful JavaScript plugins.</p>
      <div class="d-grid gap-2 d-md-flex justify-content-md-start">
        <button type="button" class="btn btn-dark btn-lg px-4 me-md-2">Explore</button>
      </div>
    </div>
  </div>
</div>
</section>

<section id="products">
  <div class="container-fluid products">
    <div class="row">
      <div class="col-md-5 p-lg-5 mx-auto my-5" >
        <h1 class="display-4 fw-normal">Our Products</h1>
        <p class="lead fw-normal">And an even wittier subheading to boot. Jumpstart your marketing efforts with this example based on Apple’s marketing pages.</p>
        <a class="btn btn-outline-secondary" href="#particles-js">Coming soon</a>
      </div>
      <div class="col-md-7 col-sm-7">
      </div>
    </div>
  </div>

  <div class="container">
  <div class="row row-cols-1 row-cols-lg-3 align-items-stretch g-4 py-5">
      <div class="col">
      <div class="card1">
        <input type="checkbox" id="card1" class="more" aria-hidden="true" />
          <div class="content">
            <div class="front" style="background-image: url('https://images.unsplash.com/photo-1529408686214-b48b8532f72c?ixlib=rb-0.3.5&ixid=eyJhcHBfaWQiOjEyMDd9&s=986e2dee5c1b488d877ad7ba1afaf2ec&auto=format&fit=crop&w=1350&q=80')">
              <div class="inner">
                <h2>Applikasi</h2>
                <div class="rating">
                  <i class="fas fa-star"></i>
                  <i class="fas fa-star"></i>
                  <i class="fas fa-star"></i>
                  <i class="fas fa-star"></i>
                  <i class="far fa-star"></i>
                </div>
                <label for="card1" class="button" aria-hidden="true"> Details </label>
              </div>
            </div>
            <div class="back">
              <div class="inner">
                <div class="description">
                  <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Vitae, accusamus.</p>
                  <p>
                    Lorem ipsum dolor sit, amet consectetur adipisicing elit. Voluptates earum nostrum ipsam ullam, reiciendis nam consectetur? Doloribus voluptate architecto possimus perferendis tenetur nemo amet temporibus, enim soluta nam,
                    debitis.
                  </p>
                </div>
                <div class="location">Mobile Legends</div>
                <div class="price">200k</div>
                <label for="card1" class="button return" aria-hidden="true">
                  <i class="fas fa-arrow-left"></i>
                </label>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div class="col">
      <div class="card1">
        <input type="checkbox" id="card2" class="more" aria-hidden="true" />
          <div class="content">
            <div class="front" style="background-image: url('https://images.unsplash.com/photo-1529408686214-b48b8532f72c?ixlib=rb-0.3.5&ixid=eyJhcHBfaWQiOjEyMDd9&s=986e2dee5c1b488d877ad7ba1afaf2ec&auto=format&fit=crop&w=1350&q=80')">
              <div class="inner">
                <h2>Applikasi</h2>
                <div class="rating">
                  <i class="fas fa-star"></i>
                  <i class="fas fa-star"></i>
                  <i class="fas fa-star"></i>
                  <i class="fas fa-star"></i>
                  <i class="far fa-star"></i>
                </div>
                <label for="card2" class="button" aria-hidden="true"> Details </label>
              </div>
            </div>
            <div class="back">
              <div class="inner">
                <div class="description">
                  <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Vitae, accusamus.</p>
                  <p>
                    Lorem ipsum dolor sit, amet consectetur adipisicing elit. Voluptates earum nostrum ipsam ullam, reiciendis nam consectetur? Doloribus voluptate architecto possimus perferendis tenetur nemo amet temporibus, enim soluta nam,
                    debitis.
                  </p>
                </div>
                <div class="location">Mobile Legends</div>
                <div class="price">200k</div>
                <label for="card2" class="button return" aria-hidden="true">
                  <i class="fas fa-arrow-left"></i>
                </label>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div class="col">
      <div class="card1">
        <input type="checkbox" id="card3" class="more" aria-hidden="true" />
          <div class="content">
            <div class="front" style="background-image: url('https://images.unsplash.com/photo-1529408686214-b48b8532f72c?ixlib=rb-0.3.5&ixid=eyJhcHBfaWQiOjEyMDd9&s=986e2dee5c1b488d877ad7ba1afaf2ec&auto=format&fit=crop&w=1350&q=80')">
              <div class="inner">
                <h2>Applikasi</h2>
                <div class="rating">
                  <i class="fas fa-star"></i>
                  <i class="fas fa-star"></i>
                  <i class="fas fa-star"></i>
                  <i class="fas fa-star"></i>
                  <i class="far fa-star"></i>
                </div>
                <label for="card3" class="button" aria-hidden="true"> Details </label>
              </div>
            </div>
            <div class="back">
              <div class="inner">
                <div class="description">
                  <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Vitae, accusamus.</p>
                  <p>
                    Lorem ipsum dolor sit, amet consectetur adipisicing elit. Voluptates earum nostrum ipsam ullam, reiciendis nam consectetur? Doloribus voluptate architecto possimus perferendis tenetur nemo amet temporibus, enim soluta nam,
                    debitis.
                  </p>
                </div>
                <div class="location">Mobile Legends</div>
                <div class="price">200k</div>
                <label for="card3" class="button return" aria-hidden="true">
                  <i class="fas fa-arrow-left"></i>
                </label>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div class="col">
      <div class="card1">
        <input type="checkbox" id="card4" class="more" aria-hidden="true" />
          <div class="content">
            <div class="front" style="background-image: url('https://images.unsplash.com/photo-1529408686214-b48b8532f72c?ixlib=rb-0.3.5&ixid=eyJhcHBfaWQiOjEyMDd9&s=986e2dee5c1b488d877ad7ba1afaf2ec&auto=format&fit=crop&w=1350&q=80')">
              <div class="inner">
                <h2>Applikasi</h2>
                <div class="rating">
                  <i class="fas fa-star"></i>
                  <i class="fas fa-star"></i>
                  <i class="fas fa-star"></i>
                  <i class="fas fa-star"></i>
                  <i class="far fa-star"></i>
                </div>
                <label for="card4" class="button" aria-hidden="true"> Details </label>
              </div>
            </div>
            <div class="back">
              <div class="inner">
                <div class="description">
                  <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Vitae, accusamus.</p>
                  <p>
                    Lorem ipsum dolor sit, amet consectetur adipisicing elit. Voluptates earum nostrum ipsam ullam, reiciendis nam consectetur? Doloribus voluptate architecto possimus perferendis tenetur nemo amet temporibus, enim soluta nam,
                    debitis.
                  </p>
                </div>
                <div class="location">Mobile Legends</div>
                <div class="price">200k</div>
                <label for="card4" class="button return" aria-hidden="true">
                  <i class="fas fa-arrow-left"></i>
                </label>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div class="col">
      <div class="card1">
        <input type="checkbox" id="card5" class="more" aria-hidden="true" />
          <div class="content">
            <div class="front" style="background-image: url('https://images.unsplash.com/photo-1529408686214-b48b8532f72c?ixlib=rb-0.3.5&ixid=eyJhcHBfaWQiOjEyMDd9&s=986e2dee5c1b488d877ad7ba1afaf2ec&auto=format&fit=crop&w=1350&q=80')">
              <div class="inner">
                <h2>Applikasi</h2>
                <div class="rating">
                  <i class="fas fa-star"></i>
                  <i class="fas fa-star"></i>
                  <i class="fas fa-star"></i>
                  <i class="fas fa-star"></i>
                  <i class="far fa-star"></i>
                </div>
                <label for="card5" class="button" aria-hidden="true"> Details </label>
              </div>
            </div>
            <div class="back">
              <div class="inner">
                <div class="description">
                  <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Vitae, accusamus.</p>
                  <p>
                    Lorem ipsum dolor sit, amet consectetur adipisicing elit. Voluptates earum nostrum ipsam ullam, reiciendis nam consectetur? Doloribus voluptate architecto possimus perferendis tenetur nemo amet temporibus, enim soluta nam,
                    debitis.
                  </p>
                </div>
                <div class="location">Mobile Legends</div>
                <div class="price">200k</div>
                <label for="card5" class="button return" aria-hidden="true">
                  <i class="fas fa-arrow-left"></i>
                </label>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div class="col">
      <div class="card1">
        <input type="checkbox" id="card6" class="more" aria-hidden="true" />
          <div class="content">
            <div class="front" style="background-image: url('https://images.unsplash.com/photo-1529408686214-b48b8532f72c?ixlib=rb-0.3.5&ixid=eyJhcHBfaWQiOjEyMDd9&s=986e2dee5c1b488d877ad7ba1afaf2ec&auto=format&fit=crop&w=1350&q=80')">
              <div class="inner">
                <h2>Applikasi</h2>
                <div class="rating">
                  <i class="fas fa-star"></i>
                  <i class="fas fa-star"></i>
                  <i class="fas fa-star"></i>
                  <i class="fas fa-star"></i>
                  <i class="far fa-star"></i>
                </div>
                <label for="card6" class="button" aria-hidden="true"> Details </label>
              </div>
            </div>
            <div class="back">
              <div class="inner">
                <div class="description">
                  <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Vitae, accusamus.</p>
                  <p>
                    Lorem ipsum dolor sit, amet consectetur adipisicing elit. Voluptates earum nostrum ipsam ullam, reiciendis nam consectetur? Doloribus voluptate architecto possimus perferendis tenetur nemo amet temporibus, enim soluta nam,
                    debitis.
                  </p>
                </div>
                <div class="location">Mobile Legends</div>
                <div class="price">200k</div>
                <label for="card6" class="button return" aria-hidden="true">
                  <i class="fas fa-arrow-left"></i>
                </label>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>

<section id="pricing">
<div class="container">
<div class="pricing-header p-3 mt-3 pb-md-4 mx-auto text-center">
  <h1 class="display-4 fw-normal">Pricing</h1>
  <p class="fs-5 text-muted">Quickly build an effective pricing table for your potential customers with this Bootstrap example. It’s built with default Bootstrap components and utilities with little customization.</p>
</div>
<div class="row row-cols-1 mb-5">
  <div class="promos">
    <div class="promo first">
      <h4>Basic</h4>
      <ul class="features">
          <li class="brief">Basic membership</li>
          <li class="price">$69</span></li>
          <li>Some great feature</li>
          <li>Another super feature</li>
          <li>And more...</li>
          <li class="buy"><button>Sign up</button></li>   
      </ul>
  </div>
  
    <div class="promo second">
      <h4>Plus</h4>
      <ul class="features">
          <li class="brief">Plus membership</li>
          <li class="price">$79</li>
          <li>Some great feature</li>
          <li>Another super feature</li>
          <li>And more...</li> 
          <li class="buy"><button>Sign up</button></li>  
      </ul>
  </div>

    <div class="promo third scale">
      <h4>Premium</h4>
        <ul class="features">
          <li class="brief">This is really a good deal!</li>
          <li class="price">$89</li>
          <li>Some great feature</li>
          <li>Another super feature</li>
          <li>And more...</li>  
          <li class="buy"><button>Sign up</button></li> 
        </ul>
      </div>  
   </div>
 </div>
</div>
</section>

<section style="margin-top: 100px;">
  <div class="container-fluid">
    <div class="row d-flex justify-content-center testimonial-pos">
        <div class="col-md-12 pt-4 d-flex justify-content-center">
            <h3>Testimonials</h3>
        </div>
        <div class="col-md-12 d-flex justify-content-center text-center">
            <h2>Explore the customers experience</h2>
        </div>
    </div>
  <div id="carouselMultiItemExample" class="carousel slide carousel-dark text-center" data-mdb-ride="carousel">
    <div class="carousel-inner py-4">
      <div class="carousel-item active">
        <div class="container">
          <div class="row">
            <div class="col-lg-4">
              <img class="rounded-circle shadow-1-strong mb-4"
                src="https://mdbcdn.b-cdn.net/img/Photos/Avatars/img%20(1).webp" alt="avatar"
                style="width: 150px;" />
              <h5 class="mb-3">Anna Deynah</h5>
              <p>UX Designer</p>
              <p class="text-muted">
                <i class="fas fa-quote-left pe-2"></i>
                Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quod eos id
                officiis hic tenetur quae quaerat ad velit ab hic tenetur.
              </p>
              <ul class="list-unstyled d-flex justify-content-center text-warning mb-0">
                <li><i class="fas fa-star fa-sm"></i></li>
                <li><i class="fas fa-star fa-sm"></i></li>
                <li><i class="fas fa-star fa-sm"></i></li>
                <li><i class="fas fa-star fa-sm"></i></li>
                <li><i class="fas fa-star fa-sm"></i></li>
              </ul>
            </div>
  
            <div class="col-lg-4 d-none d-lg-block">
              <img class="rounded-circle shadow-1-strong mb-4"
                src="https://mdbcdn.b-cdn.net/img/Photos/Avatars/img%20(32).webp" alt="avatar"
                style="width: 150px;" />
              <h5 class="mb-3">John Doe</h5>
              <p>Web Developer</p>
              <p class="text-muted">
                <i class="fas fa-quote-left pe-2"></i>
                Ut enim ad minima veniam, quis nostrum exercitationem ullam corporis
                suscipit laboriosam, nisi ut aliquid commodi.
              </p>
              <ul class="list-unstyled d-flex justify-content-center text-warning mb-0">
                <li><i class="fas fa-star fa-sm"></i></li>
                <li><i class="fas fa-star fa-sm"></i></li>
                <li><i class="fas fa-star fa-sm"></i></li>
                <li><i class="fas fa-star fa-sm"></i></li>
                <li>
                  <i class="fas fa-star-half-alt fa-sm"></i>
                </li>
              </ul>
            </div>
  
            <div class="col-lg-4 d-none d-lg-block">
              <img class="rounded-circle shadow-1-strong mb-4"
                src="https://mdbcdn.b-cdn.net/img/Photos/Avatars/img%20(10).webp" alt="avatar"
                style="width: 150px;" />
              <h5 class="mb-3">Maria Kate</h5>
              <p>Photographer</p>
              <p class="text-muted">
                <i class="fas fa-quote-left pe-2"></i>
                At vero eos et accusamus et iusto odio dignissimos ducimus qui blanditiis
                praesentium voluptatum deleniti atque corrupti.
              </p>
              <ul class="list-unstyled d-flex justify-content-center text-warning mb-0">
                <li><i class="fas fa-star fa-sm"></i></li>
                <li><i class="fas fa-star fa-sm"></i></li>
                <li><i class="fas fa-star fa-sm"></i></li>
                <li><i class="fas fa-star fa-sm"></i></li>
                <li><i class="far fa-star fa-sm"></i></li>
              </ul>
            </div>
          </div>
        </div>
      </div>
  
      <div class="carousel-item">
        <div class="container">
          <div class="row">
            <div class="col-lg-4">
              <img class="rounded-circle shadow-1-strong mb-4"
                src="https://mdbcdn.b-cdn.net/img/Photos/Avatars/img%20(3).webp" alt="avatar"
                style="width: 150px;" />
              <h5 class="mb-3">John Doe</h5>
              <p>UX Designer</p>
              <p class="text-muted">
                <i class="fas fa-quote-left pe-2"></i>
                Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quod eos id
                officiis hic tenetur quae quaerat ad velit ab hic tenetur.
              </p>
              <ul class="list-unstyled d-flex justify-content-center text-warning mb-0">
                <li><i class="fas fa-star fa-sm"></i></li>
                <li><i class="fas fa-star fa-sm"></i></li>
                <li><i class="fas fa-star fa-sm"></i></li>
                <li><i class="fas fa-star fa-sm"></i></li>
                <li><i class="fas fa-star fa-sm"></i></li>
              </ul>
            </div>
  
            <div class="col-lg-4 d-none d-lg-block">
              <img class="rounded-circle shadow-1-strong mb-4"
                src="https://mdbcdn.b-cdn.net/img/Photos/Avatars/img%20(4).webp" alt="avatar"
                style="width: 150px;" />
              <h5 class="mb-3">Alex Rey</h5>
              <p>Web Developer</p>
              <p class="text-muted">
                <i class="fas fa-quote-left pe-2"></i>
                Ut enim ad minima veniam, quis nostrum exercitationem ullam corporis
                suscipit laboriosam, nisi ut aliquid commodi.
              </p>
              <ul class="list-unstyled d-flex justify-content-center text-warning mb-0">
                <li><i class="fas fa-star fa-sm"></i></li>
                <li><i class="fas fa-star fa-sm"></i></li>
                <li><i class="fas fa-star fa-sm"></i></li>
                <li><i class="fas fa-star fa-sm"></i></li>
                <li>
                  <i class="fas fa-star-half-alt fa-sm"></i>
                </li>
              </ul>
            </div>
  
            <div class="col-lg-4 d-none d-lg-block">
              <img class="rounded-circle shadow-1-strong mb-4"
                src="https://mdbcdn.b-cdn.net/img/Photos/Avatars/img%20(5).webp" alt="avatar"
                style="width: 150px;" />
              <h5 class="mb-3">Maria Kate</h5>
              <p>Photographer</p>
              <p class="text-muted">
                <i class="fas fa-quote-left pe-2"></i>
                At vero eos et accusamus et iusto odio dignissimos ducimus qui blanditiis
                praesentium voluptatum deleniti atque corrupti.
              </p>
              <ul class="list-unstyled d-flex justify-content-center text-warning mb-0">
                <li><i class="fas fa-star fa-sm"></i></li>
                <li><i class="fas fa-star fa-sm"></i></li>
                <li><i class="fas fa-star fa-sm"></i></li>
                <li><i class="fas fa-star fa-sm"></i></li>
                <li><i class="far fa-star fa-sm"></i></li>
              </ul>
            </div>
          </div>
        </div>
      </div>
  
      <div class="carousel-item">
        <div class="container">
          <div class="row">
            <div class="col-lg-4">
              <img class="rounded-circle shadow-1-strong mb-4"
                src="https://mdbcdn.b-cdn.net/img/Photos/Avatars/img%20(6).webp" alt="avatar"
                style="width: 150px;" />
              <h5 class="mb-3">Anna Deynah</h5>
              <p>UX Designer</p>
              <p class="text-muted">
                <i class="fas fa-quote-left pe-2"></i>
                Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quod eos id
                officiis hic tenetur quae quaerat ad velit ab hic tenetur.
              </p>
              <ul class="list-unstyled d-flex justify-content-center text-warning mb-0">
                <li><i class="fas fa-star fa-sm"></i></li>
                <li><i class="fas fa-star fa-sm"></i></li>
                <li><i class="fas fa-star fa-sm"></i></li>
                <li><i class="fas fa-star fa-sm"></i></li>
                <li><i class="fas fa-star fa-sm"></i></li>
              </ul>
            </div>
  
            <div class="col-lg-4 d-none d-lg-block">
              <img class="rounded-circle shadow-1-strong mb-4"
                src="https://mdbcdn.b-cdn.net/img/Photos/Avatars/img%20(8).webp" alt="avatar"
                style="width: 150px;" />
              <h5 class="mb-3">John Doe</h5>
              <p>Web Developer</p>
              <p class="text-muted">
                <i class="fas fa-quote-left pe-2"></i>
                Ut enim ad minima veniam, quis nostrum exercitationem ullam corporis
                suscipit laboriosam, nisi ut aliquid commodi.
              </p>
              <ul class="list-unstyled d-flex justify-content-center text-warning mb-0">
                <li><i class="fas fa-star fa-sm"></i></li>
                <li><i class="fas fa-star fa-sm"></i></li>
                <li><i class="fas fa-star fa-sm"></i></li>
                <li><i class="fas fa-star fa-sm"></i></li>
                <li>
                  <i class="fas fa-star-half-alt fa-sm"></i>
                </li>
              </ul>
            </div>
  
            <div class="col-lg-4 d-none d-lg-block">
              <img class="rounded-circle shadow-1-strong mb-4"
                src="https://mdbcdn.b-cdn.net/img/Photos/Avatars/img%20(7).webp" alt="avatar"
                style="width: 150px;" />
              <h5 class="mb-3">Maria Kate</h5>
              <p>Photographer</p>
              <p class="text-muted">
                <i class="fas fa-quote-left pe-2"></i>
                At vero eos et accusamus et iusto odio dignissimos ducimus qui blanditiis
                praesentium voluptatum deleniti atque corrupti.
              </p>
              <ul class="list-unstyled d-flex justify-content-center text-warning mb-0">
                <li><i class="fas fa-star fa-sm"></i></li>
                <li><i class="fas fa-star fa-sm"></i></li>
                <li><i class="fas fa-star fa-sm"></i></li>
                <li><i class="fas fa-star fa-sm"></i></li>
                <li><i class="far fa-star fa-sm"></i></li>
              </ul>
            </div>
          </div>
        </div>
      </div>
    </div>
    <div class="d-flex justify-content-center" style="margin-top: -130px;">
      <button class="carousel-control-prev position-relative" type="button"
        data-mdb-target="#carouselMultiItemExample" data-mdb-slide="prev">
        <span class="carousel-control-prev-icon" aria-hidden="true"></span>
        <span class="visually-hidden">Previous</span>
      </button>
      <button class="carousel-control-next position-relative" type="button"
        data-mdb-target="#carouselMultiItemExample" data-mdb-slide="next">
        <span class="carousel-control-next-icon" aria-hidden="true"></span>
        <span class="visually-hidden">Next</span>
      </button>
    </div>
  </div>
</section>


<section id="contact">
<div class="form-area mb-4" id="contact">
  <div class="container">
      <div class="row single-form g-0">
          <div class="col-sm-12 col-lg-6">
              <div class="left">
                  <h2><span>Contact Us for</span> <br><span class="kedip">Business Enquies</span></h2>
              </div>
          </div>
          <div class="col-sm-12 col-lg-6">
              <div class="right">
                 <i class="fa fa-caret-left"></i>
                  <form>
                    <div class="mb-3">
                      <label for="exampleInputEmail1" class="form-label">Your Name</label>
                      <input type="text" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp">
                    </div>
                    <div class="mb-3">
                      <label for="exampleInputEmail1" class="form-label">Email address</label>
                      <input type="email" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp">
                    </div>
                    <div class="mb-3">
                      <label for="exampleInputPassword1" class="form-label">Message</label>
                        <textarea type="password" class="form-control" id="exampleInputPassword1"></textarea>
                    </div>
                    <button type="submit" class="btn btn-primary">Submit</button>
                  </form>
              </div>
          </div>
      </div>
  </div>
</div>
</section>

<footer class="text-center text-lg-start text-white" style="background-color: #1c2331">

<section class="d-flex justify-content-between p-4" style="background-color: #1c2331">

<div class="me-5">
<span>Get connected with us on social networks:</span>
</div>

<div>
<a href="" class="text-white me-4">
<i class="fab fa-facebook-f"></i>
</a>
<a href="" class="text-white me-4">
<i class="fab fa-twitter"></i>
</a>
<a href="" class="text-white me-4">
<i class="fab fa-google"></i>
</a>
<a href="" class="text-white me-4">
<i class="fab fa-instagram"></i>
</a>
<a href="" class="text-white me-4">
<i class="fab fa-linkedin"></i>
</a>
<a href="" class="text-white me-4">
<i class="fab fa-github"></i>
</a>
</div>
</section>

<section>
<div class="container text-center text-md-start mt-5">
<div class="row mt-3">
<div class="col-md-3 col-lg-4 col-xl-3 mx-auto mb-4">
  <h6 class="text-uppercase fw-bold">Company name</h6>
  <hr
      class="mb-4 mt-0 d-inline-block mx-auto"
      style="width: 60px; background-color: #7c4dff; height: 2px"
      />
  <p>
    Here you can use rows and columns to organize your footer
    content. Lorem ipsum dolor sit amet, consectetur adipisicing
    elit.
  </p>
</div>
<div class="col-md-2 col-lg-2 col-xl-2 mx-auto mb-4">
  <h6 class="text-uppercase fw-bold">Products</h6>
  <hr
      class="mb-4 mt-0 d-inline-block mx-auto"
      style="width: 60px; background-color: #7c4dff; height: 2px"/>
  <p>
    <a href="#!" class="text-white">MDBootstrap</a>
  </p>
  <p>
    <a href="#!" class="text-white">MDWordPress</a>
  </p>
  <p>
    <a href="#!" class="text-white">BrandFlow</a>
  </p>
  <p>
    <a href="#!" class="text-white">Bootstrap Angular</a>
  </p>
</div>


<div class="col-md-3 col-lg-2 col-xl-2 mx-auto mb-4">
  <h6 class="text-uppercase fw-bold">Useful links</h6>
  <hr
      class="mb-4 mt-0 d-inline-block mx-auto"
      style="width: 60px; background-color: #7c4dff; height: 2px"/>
  <p>
    <a href="#!" class="text-white">Your Account</a>
  </p>
  <p>
    <a href="#!" class="text-white">Become an Affiliate</a>
  </p>
  <p>
    <a href="#!" class="text-white">Shipping Rates</a>
  </p>
  <p>
    <a href="#!" class="text-white">Help</a>
  </p>
</div>

<div class="col-md-4 col-lg-3 col-xl-3 mx-auto mb-md-0 mb-4">
  <h6 class="text-uppercase fw-bold">Contact</h6>
  <hr
      class="mb-4 mt-0 d-inline-block mx-auto"
      style="width: 60px; background-color: #7c4dff; height: 2px"
      />
  <p><i class="fas fa-home mr-3"></i> New York, NY 10012, US</p>
  <p><i class="fas fa-envelope mr-3"></i> info@example.com</p>
  <p><i class="fas fa-phone mr-3"></i> + 01 234 567 88</p>
  <p><i class="fas fa-print mr-3"></i> + 01 234 567 89</p>
</div>
</div>
</div>
</section>


<div class="text-center p-3" style="background-color: rgba(0, 0, 0, 0.2)">
© 2023 Copyright:
<a class="text-white" href="#">Meyaboo</a>
</div>

</footer>
</div>
<a href="#home" id="toTopBtn" class="" data-abc="true"><i class="fa-sharp fa-solid fa-arrow-up text-light" style="font-size: 25px"></i></a>



@endsection




