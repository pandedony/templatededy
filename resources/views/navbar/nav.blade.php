<style>
.navbar-scrolled{
    background:black;
    box-shadow: 0 3px 10px rgba(0, 0, 0, 0.692);

}
.carousel {
    margin-bottom: 4rem;
}
.carousel-caption {
    bottom: 3rem;
    z-index: 10;
}
.carousel-item {
    height: 32rem;
}
.marketing .col-lg-4 {
    margin-bottom: 1.5rem;
    text-align: center;
}
.marketing .col-lg-4 p {
    margin-right: 0.75rem;
    margin-left: 0.75rem;
}
.featurette-divider {
    margin: 5rem 0; 
}
.featurette-heading {
    letter-spacing: -0.05rem;
}
@media (min-width: 40em) {
    .carousel-caption p {
        margin-bottom: 1.25rem;
        font-size: 1.25rem;
        line-height: 1.4;
    }

    .featurette-heading {
        font-size: 50px;
    }
}
@media (min-width: 62em) {
    .featurette-heading {
        margin-top: 7rem;
    }
}
</style>  
<header>
    <nav class="navbar fixed-top navbar-expand-lg navbar-dark">
        <div class="container-fluid">
          <a class="navbar-brand" href="/">Meyaboo</a>
          <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
          </button>
          <div class="collapse navbar-collapse" id="navbarNav">
            <ul class="navbar-nav">
              <li class="nav-item">
                <a class="nav-link active" aria-current="page" href="#particles-js"><i class="fa-sharp fa-solid fa-house"></i> Home</a>
              </li>
              <li class="nav-item">
                <a class="nav-link" href="#about"><i class="fa-sharp fa-solid fa-magnifying-glass"></i> About</a>
              </li>
              <li class="nav-item">
                <a class="nav-link" href="#products"><i class="fa-sharp fa-solid fa-bag-shopping"></i> Products</a>
              </li>
              <li class="nav-item">
                <a class="nav-link" href="#pricing"><i class="fa-sharp fa-solid fa-tag"></i> Pricing</a>
              </li>
              <li class="nav-item">
                <a class="nav-link" href="#contact"><i class="fa-sharp fa-solid fa-address-book"></i> Contact Us</a>
              </li>

            </ul>
            <ul class="navbar-nav ms-auto">
            <div class="">
                <button type="button" class="btn btn-light me-2">Login</button>
                <button type="button" class="btn btn-dark">Sign-up</button>
              </div>
            </ul>
          </div>
        </div>
      </nav>
</header>

<div class="container-fluid" id="particles-js"></div>